package com.ittipon.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.ittipon.diceroller.Dice

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as  NavHostFragment
        val navController = navHostFragment.navController
        setupActionBarWithNavController(navController )
    }

    
    private fun rollDice() {
        val dice = Dice(6)
        val diceRoll = dice.roll()
        val diceImage: ImageView = findViewById(R.id.imageView)
        val drawableResource = when (diceRoll) {
            1 -> {R.drawable.dice1 }
            2 -> {R.drawable.dice2 }
            3 -> {R.drawable.dice3 }
            4 -> {R.drawable.dice4 }
            5 -> {R.drawable.dice5 }
            6 -> {
                R.drawable.dice6
            }
            else -> R.drawable.dice6
        }
        diceImage.setImageResource(drawableResource)
        diceImage.contentDescription = diceRoll.toString()
    }






}