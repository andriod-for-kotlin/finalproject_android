package com.ittipon.myapplication

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ittipon.myapplication.databinding.FragmentMainPlayBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.concurrent.schedule

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MainPlayFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainPlayFragment : Fragment() {

    // TODO: Rename and change types of parameters

    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentMainPlayBinding? = null
    private val binding get() = _binding
    private var turn = 0
    private var position1 = 0
    private var position2 = 0
    private val IntArray.up: Boolean get() = contains(4) || contains(9) || contains(20)
            || contains(28) || contains(40) || contains(51) || contains(63)
    private val IntArray.down: Boolean get() = contains(17) || contains(64) || contains(89)
            || contains(95) || contains(99)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            turn = it.getInt("turn")
            position1 = it.getInt("position1")
            position2 = it.getInt("position2")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMainPlayBinding.inflate(inflater, container , false)
        return binding?.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setNewStateGame()
        checkPosition1SnakeOrStair()
        checkPosition2SnakeOrStair()

        binding?.btnNext?.setOnClickListener{
            val action = MainPlayFragmentDirections.actionMainPlayFragmentToRandomDiceFragment(position1,position2,turn)
            view.findNavController().navigate(action)
        }

    }

    private fun checkPosition1SnakeOrStair() {
        if (intArrayOf(position1).up) {
    //            Log.d("Cal","True")

            val newPosition1 = findPositionUpEnd(position1)
            showUpStairDialog("player1",position1,newPosition1)
            position1 = newPosition1
            binding?.textPositionPlayer1?.text = newPosition1.toString()

        } else if (intArrayOf(position1).down) {
    //            Log.d("Cal","False")
            val newPosition1 = findPositionDownEnd(position1)
            showDownSnakeDialog("player1",position1,newPosition1)
            position1 = newPosition1
            binding?.textPositionPlayer1?.text = newPosition1.toString()

        }
    }

    private fun checkPosition2SnakeOrStair() {
        if (intArrayOf(position2).up) {
            //            Log.d("Cal","True")
            val newPosition2 = findPositionUpEnd(position2)
            showUpStairDialog("player2",position2,newPosition2)
            position2 = newPosition2
            binding?.textPositionPlayer2?.text = newPosition2.toString()

        } else if (intArrayOf(position2).down) {
            //            Log.d("Cal","False")
            val newPosition2 = findPositionDownEnd(position1)
            showDownSnakeDialog("player2",position2,newPosition2)
            position2 = newPosition2
            binding?.textPositionPlayer2?.text = newPosition2.toString()

        }
    }

    private fun setNewStateGame() {
        turn++
        binding?.textPositionPlayer1?.text = position1.toString()
        binding?.textPositionPlayer2?.text = position2.toString()
    }

    private fun findPositionUpEnd(position : Int) : Int {
        return when (position) {
            4 -> 14
            9 -> 31
            20 -> 38
            28 -> 84
            40 -> 59
            51 -> 67
            else -> 81
        }
    }

    private fun findPositionDownEnd(position : Int) : Int {
        return when (position) {
            17 -> 7
            64 -> 60
            89 -> 26
            95 -> 75
            else -> 78
        }
    }

    private fun showUpStairDialog(name: String,oldPosition : Int,newPosition : Int) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.notification,name))
            .setMessage(getString(R.string.upStairMassages, oldPosition, newPosition))
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
            }
            .show()
    }

    private fun showDownSnakeDialog(name: String,oldPosition : Int,newPosition : Int) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.notification,name))
            .setMessage(getString(R.string.downSnakeMassages, oldPosition, newPosition))
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
            }
            .show()
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment mainPlayFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MainPlayFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}