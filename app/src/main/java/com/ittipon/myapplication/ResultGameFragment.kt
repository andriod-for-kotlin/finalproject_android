package com.ittipon.myapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.ittipon.myapplication.databinding.FragmentMainPlayBinding
import com.ittipon.myapplication.databinding.FragmentResultGameBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ResultGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ResultGameFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentResultGameBinding? = null
    private val binding get() = _binding
    private var winner = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            winner = it.getBoolean("winner")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.d("Winner" , "1 is Winner : $winner")
        _binding = FragmentResultGameBinding.inflate(inflater, container , false)
        return binding?.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.btnRestart?.setOnClickListener(){
            val action = ResultGameFragmentDirections.actionResultGameFragmentToStartGameFragment()
            view.findNavController().navigate(action)
        }

//        Log.d("Winner" , "1 is Winner : $winner")
        if(winner){
            binding?.textResultPlayer1?.text = "Win !!!"
            binding?.textResultPlayer2?.text = "Lose !!!"
            binding?.textResultPlayer1?.setTextColor(0xff00B307.toInt())
            binding?.textResultPlayer2?.setTextColor(0xffAE0505.toInt())

        }else {
            binding?.textResultPlayer2?.text = "Win !!!"
            binding?.textResultPlayer1?.text = "Lose !!!"
            binding?.textResultPlayer2?.setTextColor(0xff00B307.toInt())
            binding?.textResultPlayer1?.setTextColor(0xffAE0505.toInt())

        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ResultGameFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ResultGameFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}