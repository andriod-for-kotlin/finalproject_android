package com.ittipon.myapplication


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.ittipon.diceroller.Dice
import com.ittipon.myapplication.databinding.FragmentRandomDiceBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RandomDiceFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RandomDiceFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var winner = false
    private var turn = 0
    private var position1 = 0
    private var position2 = 0

    private var _binding: FragmentRandomDiceBinding? = null
    private val binding get() = _binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            turn = it.getInt("turn")
            position1 = it.getInt("position1")
            position2 = it.getInt("position2")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRandomDiceBinding.inflate(inflater, container , false)
        return binding?.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnRandom = binding?.btnRandom
        val btnNext = binding?.btnNext
        changeTurn()
        hideButton(btnNext)
        binding?.btnRandom?.setOnClickListener{
            movePositionOfPlayer()
            hideButton(btnRandom)
            showButton(btnNext)
        }



        binding?.btnNext?.setOnClickListener {
            if(checkResult()){
                winner = checkPlayer1Win()
                Log.d("Winner" , "1 is Winner : $winner")
                val action = RandomDiceFragmentDirections.actionRandomDiceFragmentToResultGameFragment(winner)
                view.findNavController().navigate(action)
            }else {
                val action = RandomDiceFragmentDirections.actionRandomDiceFragmentToMainPlayFragment(position1,position2,turn)
                view.findNavController().navigate(action)
            }

        }


    }

    private fun changeTurn() {
        if (turn % 2 == 0) {
            binding?.textPlayer?.text = "Player 2"
        }
    }

    private fun movePositionOfPlayer() {
        val pointDice = rollAndLoadDice()
        if (turn % 2 == 1) {
            position1 = position1.plus(pointDice)
            Log.d("Game", "player1 $position1")
        } else {
            position2 = position2.plus(pointDice)
            Log.d("Game", "player2 $position2")
        }
    }


    private fun showButton(btnNext: Button?) {
        btnNext?.isEnabled = true
        btnNext?.setBackgroundColor(0xffFBCCE0.toInt())
        btnNext?.setTextColor(0xff000000.toInt())
    }

    private fun hideButton(btnRandom: Button?) {
        btnRandom?.isEnabled = false
        btnRandom?.setBackgroundColor(0xffC68FC8.toInt())
        btnRandom?.setTextColor(0xffC68FC8.toInt())
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RandomDiceFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RandomDiceFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun rollAndLoadDice() : Int{
        val dice = Dice(6)
        val diceRoll = dice.roll()
        val diceImage: ImageView? = binding?.imgDice
        val drawableResource = when (diceRoll) {
            1 -> {R.drawable.dice1 }
            2 -> {R.drawable.dice2 }
            3 -> {R.drawable.dice3 }
            4 -> {R.drawable.dice4 }
            5 -> {R.drawable.dice5 }
            6 -> {R.drawable.dice6 }
            else -> R.drawable.dice6
        }
        diceImage?.setImageResource(drawableResource)
        diceImage?.contentDescription = diceRoll.toString()
        return diceRoll
    }

    private fun checkResult() : Boolean {
        return position1 >=100 || position2 >= 100
    }

    private fun checkPlayer1Win() : Boolean {
        return position1>=100
    }
}